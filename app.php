<?php
require_once __DIR__ . '/vendor/autoload.php';

use MiniSearch\Commands\IndexClear;
use MiniSearch\Commands\MakeIndex;
use MiniSearch\Commands\Search;
use Symfony\Component\Console\Application;

$app = new Application();
$app->add(new MakeIndex());
$app->add(new Search());
$app->add(new IndexClear());
$app->run();