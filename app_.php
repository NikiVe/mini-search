<?php

use MiniSearch\Core;
use MiniSearch\Source\FileStore;

require_once __DIR__ . '/vendor/autoload.php';

$morphy = new \cijic\phpMorphy\Morphy('en');

$weigh = new \MiniSearch\Weigh($morphy);

$sT = microtime(true);

$core = (new Core($morphy, new \MiniSearch\Rules(['ARTICLE'])))
    ->setUseCacheIndex(true)
    ->setUseCacheIndex(__DIR__);

$index = $core->makeIndexForFileStore($fileStore = new FileStore([
//    __DIR__ . '/text2.txt',
//    __DIR__ . '/text.txt',
    __DIR__ . '/docs/LONDON_WHITE.txt',
    __DIR__ . '/docs/OLIVER_TWIST.txt',
    __DIR__ . '/docs/TWAIN_TOM_SAWYER.txt'
]));
$content = $core->search($index, new \MiniSearch\Query($morphy, 'I loved music languages'));
(new \MiniSearch\Visualization(__DIR__))->saveAsHtml($content, $fileStore);
echo 'time: ' . round(microtime(true) - $sT, 4) . PHP_EOL;