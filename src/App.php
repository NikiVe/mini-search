<?php


namespace MiniSearch;


use cijic\phpMorphy\Morphy;
use MiniSearch\Source\FileStore;

class App
{
    protected static ?self $instance = null;
    protected Core $core;

    public const DOCUMENTS_DIR = __DIR__ . '/../docs/';
    public const RESULT_DIR = __DIR__ . '/../result/';

    protected function __construct() {
        $this->core = (new Core(new Morphy('en'), new Rules()))
            ->setIndexDir(__DIR__ . '/../')
            ->setUseCacheIndex(true);
        if (!file_exists(self::DOCUMENTS_DIR)) {
            mkdir(self::DOCUMENTS_DIR);
        }
    }

    public static function get() {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getCore() {
        return $this->core;
    }

    public static function getFileStore() {
        $arFiles = scandir(self::DOCUMENTS_DIR);
        $arFilesDir = [];
        foreach ($arFiles as $fileName) {
            if (is_file(App::DOCUMENTS_DIR . $fileName))
                $arFilesDir[] = App::DOCUMENTS_DIR . $fileName;
        }
        return new FileStore($arFilesDir);
    }

    public function clearIndex() {
        if (file_exists($this->core->getPathToIndexFile())) {
            return unlink($this->core->getPathToIndexFile());
        }
        return false;
    }

    public function clearResult() {
        $arFiles = scandir(self::RESULT_DIR);
        foreach ($arFiles as $file) {
            $path = self::RESULT_DIR . $file;
            if (is_file($path)) {
                unlink($path);
            }
        }
    }
}