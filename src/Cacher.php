<?php


namespace MiniSearch;


use cijic\phpMorphy\Morphy;

class Cacher
{
    protected static string $endNode = '|';
    protected static string $endWord = '/';
    protected static string $endPosition = ',';
    protected static string $nextPosition = ';';

    public static function saveIndex(InverseIndex $index, string $path) {
        $result = '';
        /** @var Node $node */
        foreach ($index->words as $word => $node) {
            $p = '';
            /** @var Position $position */
            $c = count($node->getPositions());
            foreach ($node->getPositions() as $num => $position) {
                $p .= $position->position . self::$endPosition . $position->document;
                if ($c - 1 !== $num) {
                    $p .= self::$nextPosition;
                }
            }
            $result .= $word . self::$endWord . $p . self::$endNode;
        }
        return file_put_contents($path, $result);
    }

    public static function getFromCache($path, Morphy $morphy) : ?InverseIndex {
        $inverseIndex = new InverseIndex($morphy);
        if (file_exists($path)) {
            $content = file_get_contents($path);
            $arWords = explode(self::$endNode, $content);
            //$word = 'FORE/182,0;47078,0;67844,0;69957,0;160746,0;167823,0;185693,0;187869,0;189198,0;208330,0;223110,0;223627,0;225019,0;225226,0;228447,0;229178,0;234416,0;391005,0;70026,0';
            foreach ($arWords as $word) {
                if ($word) {
                    $wap = explode(self::$endWord, $word);
                    $arPositions = explode(self::$nextPosition, $wap[1]);
                    $node = new Node($wap[0]);
                    foreach ($arPositions as $item) {
                        $pad = explode(self::$endPosition, $item);
                        $node->addPosition(new Position((int)$pad[0], $pad[1]));
                    }
                    $inverseIndex->addNode($node);
                }
            }
            return $inverseIndex;
        }
        return null;
    }
}