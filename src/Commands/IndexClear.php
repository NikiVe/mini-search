<?php
namespace MiniSearch\Commands;

use MiniSearch\App;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class IndexClear extends Command
{
    protected function configure() {
        $this
            ->setName('index-clear')
            ->setDescription('Clearing the index')
            ->setDefinition(
                new InputDefinition([])
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        if (App::get()->clearIndex()) {
            $output->writeln('Файлы с индексом успешно удален');
            return 0;
        }
        $output->writeln('Ошибка');
        return 1;
    }
}