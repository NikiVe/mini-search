<?php
namespace MiniSearch\Commands;

use MiniSearch\App;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MakeIndex extends Command
{
    protected function configure() {
        $this
            ->setName('make-index')
            ->setDescription('Making index')
            ->setDefinition(
                new InputDefinition([])
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        App::get()->clearIndex();
        $files = App::getFileStore();
        if (!empty($files->getFileList())) {
            $sT = microtime(true);
            if (App::get()->getCore()->makeIndexForFileStore(App::getFileStore())) {
                $output->writeln('Индексакция прошла успешна! '
                    . round(microtime(true) - $sT, 4) . ' сек.');
                return 0;
            }
        }
        $output->writeln('Файлы для индекса не найдеты');
        return 0;
    }
}