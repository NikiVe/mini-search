<?php
namespace MiniSearch\Commands;

use MiniSearch\App;
use MiniSearch\Query;
use MiniSearch\Visualization;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Search extends Command
{
    protected function configure() {
        $this
            ->setName('search')
            ->setDescription('Search the index database')
            ->setDefinition(
                new InputDefinition([new InputOption('query', '', InputOption::VALUE_REQUIRED)])
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        if ($index = App::get()->getCore()->getIndexFromCache()) {
            App::get()->clearResult();

            $sT = microtime(true);

            $result = App::get()->getCore()->search(
                $index,
                new Query(App::get()->getCore()->getMorphy(), $input->getOption('query'))
            );
            (new Visualization(App::RESULT_DIR))->saveAsHtml($result, App::getFileStore());

            $output->writeln('Успех ' . round(microtime(true) - $sT, 4) . ' сек.');

            return 1;
        }
        $output->writeln('Ошибка');
        return 0;
    }
}