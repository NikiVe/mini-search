<?php


namespace MiniSearch;


use cijic\phpMorphy\Morphy;
use MiniSearch\Source\File;
use MiniSearch\Source\FileStore;
use MiniSearch\Source\ISource;

class Core
{
    protected Morphy $morphy;

    protected bool $useCacheIndex = false;

    protected string $indexDir = '';
    protected string $indexFileName = 'index.json';
    protected Rules $rules;

    public function __construct(Morphy $morphy, Rules $rules) {
        $this->morphy = $morphy;
        $this->rules = $rules;
    }

    public function getPathToIndexFile() {
        return $this->indexDir . $this->indexFileName;
    }

    public function getMorphy() {
        return $this->morphy;
    }

    public function setIndexDir(string $indexDir) {
        $this->indexDir = $indexDir;
        return $this;
    }

    public function setUseCacheIndex(bool $flag) {
        $this->useCacheIndex = $flag;
        return $this;
    }

    public function makeIndexForFileStore(FileStore $fileStore) {
        if ($this->useCacheIndex) {
            if ($index = $this->getIndexFromCache()) {
                return $index;
            }
        }
        $index = new InverseIndex($this->morphy);

        /** @var File $file */
        foreach ($fileStore->getFileList() as $file) {
            $index = $this->makeIndex($file, $index);
        }
        $index->convertToBasic();
        if ($this->useCacheIndex) {
            $this->saveIndexToCache($index);
        }
        return $index;
    }

    public function makeIndex(ISource $source, InverseIndex $index = null) {
        if (!$index) {
            $index = new InverseIndex($this->morphy);
        }

        /** @var Word $word */
        while ($word = $source->getWord()) {
            $w = mb_strtoupper($word->word);
            $index->addWord($w, new Position($word->position, $source->getSourceId()));
        }
        return $index;
    }

    public function search(InverseIndex $index, Query $query) {
        $baseQueryWords = [];
        $positions = [];

        $arQueryWords = $query->getQuery();
        foreach ($arQueryWords as $qw) {
            $baseQueryWords[] = $index->getWordByBaseWord($qw);
        }

        /** @var Node $item */
        foreach ($baseQueryWords as $item) {
            if ($item) {
                $positions = array_merge($positions, $item->getPositions());
            }
        }

        uasort($positions, function (Position $a, Position $b) {
            return $a->position > $b->position;
        });

        return ResultChain::get($positions);
    }

    public function saveIndexToCache(InverseIndex $index) {
        return Cacher::saveIndex($index, $this->getPathToIndexFile());;
    }

    public function getIndexFromCache() {
        return Cacher::getFromCache($this->getPathToIndexFile(), $this->morphy);
    }
}