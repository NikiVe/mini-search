<?php


namespace MiniSearch;


interface IIndex
{
    public function addWord(string $word, int $index);
    public function is_set() : bool;
    public function getIndex(string $word) : int;
    public function unsetWord(string $word);
    public function getCount() : int;
}