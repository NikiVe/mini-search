<?php


namespace MiniSearch;

use cijic\phpMorphy\Morphy;

/**
 * Class Index
 * Класс описывающий хранение слов в индексе
 *
 * @package MiniSearch
 */
class InverseIndex
{
    public int $count = 0;
    public array $words;

    protected Morphy $morphy;

    public function __construct(Morphy $morphy) {
        $this->morphy = $morphy;
    }

    public function wordIsset(string $word) {
        $basic = $this->morphy->lemmatize($word);
        foreach ($basic as $lemma) {
            if (isset($this->words[$lemma])) {
                $this->words[$lemma]->upCount();
            } else {
                return false;
            }
        }
        return true;
    }

    public function findWord(string $basicWord) {
        return $this->words[$basicWord];
    }

    public function addNode(Node $node) {
        $this->words[$node->getWord()] = $node;
    }

    public function addWord(string $word, Position $position) {
        $this->count++;
        if ($basic = [$word]) {
            foreach ($basic as $lemma) {
                if (isset($this->words[$lemma])) {
                    $this->words[$lemma]->addPosition($position);
                } else {
                    $this->words[$lemma] = (new Node($lemma))->addPosition($position);
                }
            }
        }
    }

    public function initTF(int $wordLength) {
        $sum = 0;
        /** @var Node $word */
        foreach ($this->words as $word) {
            $sum += $word->getCount();
        }

        foreach ($this->words as $word) {
            $word->setTF($word->getCount() / $wordLength);
        }
    }

    public function getWords(string $word) : array {
        $arResult = [];
        $base = $this->morphy->getBaseForm($word);
        foreach ($base as $item) {
            if (isset($this->words[$item])) {
                $arResult[] = $this->words[$item];
            }
        }
        return $arResult;
    }


    public function getWordByBaseWord(string $word) {
        return $this->words[$word] ?? null;
    }

    public function convertToBasic() {
        $basicIndex = [];
        foreach ($this->words as $word) {
            $baseForms = $this->morphy->getBaseForm($word->getWord());
            if ($baseForms) {
                foreach ($baseForms as $form) {
                    if (isset($basicIndex[$form])) {
                        $basicIndex[$form]->addPositions($word->getPositions());
                    } else {
                        $basicIndex[$form] = (new Node($form))->setPositions($word->getPositions());
                    }
                }
            } else {
                $basicIndex[$word->getWord()] = $word;
            }
        }
        $this->words = $basicIndex;
    }
}