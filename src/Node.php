<?php


namespace MiniSearch;


use cijic\phpMorphy\Morphy;

class Node
{
    protected string $wordgetBasic;
    protected int $count = 1;
    protected float $tf;

    protected array $arPositions = [];

    public function __construct(string $basic) {
        $this->word = $basic;
    }

    public function addPosition(Position $position) : self {
        $this->arPositions[] = $position;
        return $this;
    }

    public function getPositions() {
        return $this->arPositions;
    }

    public function getTF() {
        return $this->tf;
    }

    public function setTF(float $tf) {
        $this->tf = $tf;
    }

    public function getWord() {
        return $this->word;
    }

    public function getCount() {
        return $this->count;
    }

    public function upCount() {
        $this->count++;
    }

    public function setPositions(array $arPositions) {
        $this->arPositions = $arPositions;
        return $this;
    }

    public function addPositions(array $arPositions) {
        foreach ($arPositions as $position) {
            $this->arPositions[] = $position;
        }
    }
}