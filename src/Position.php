<?php


namespace MiniSearch;


class Position
{
    public int $position;
    public string $document;

    public function __construct(int $position, $document = '') {
        $this->position = $position;
        $this->document = $document;
    }
}