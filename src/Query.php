<?php


namespace MiniSearch;


use cijic\phpMorphy\Morphy;

class Query
{
    protected string $rawQuery;
    protected array $query = [];

    public function __construct(Morphy $morphy, string $query) {
        $this->rawQuery = $query;

        $words = explode(' ', $this->rawQuery);
        foreach ($words as $word) {
            $basic = $morphy->lemmatize(mb_strtoupper($word));
            $this->query = array_merge($this->query, $basic);
        }
    }

    public function getQuery() {
        return $this->query;
    }
}