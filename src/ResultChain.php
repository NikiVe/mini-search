<?php


namespace MiniSearch;


class ResultChain
{
    protected static int $maxLength = 100;

    public static function get(array $positions) {
        $result = [];
        $lastPosition = null;
        $num = 0;
        $groups = [];
        // Разбить выдачу на группы по файлам
        foreach ($positions as $p) {
            $groups[$p->document][] = $p;
        }

        // Цикл по выдаче
        $count = -1;
        foreach ($groups as $id => $group) {
            foreach ($group as $p) {
                if (isset($r[$id][$num])) {
                    $count = count($r[$id][$num]) - 1;
                    if ($count > -1) {
                        if ($p->position - $r[$id][$num][$count]->position > self::$maxLength) {
                            $num++;
                        }
                    }
                }

                // Слова в выдаче совпали
                if (isset($r[$id][$num][$count]) && $r[$id][$num][$count]->position === $p->position) {
                    continue;
                }

                $r[$id][$num][] = $p;
            }
        }

        if(isset($r)) {
            foreach ($r as $id => $group) {
                foreach ($group as $positions) {
                    $result[$id][count($positions)][] = $positions;
                }
            }
        }

        return $result;
    }
}