<?php


namespace MiniSearch;


class Rules
{
    protected array $types;

    public function __construct(array $types = []) {
        $this->types = $types;
    }

    public function getExtendsTypes() {
        return $this->types;
    }

    public function isExtend(string $type) {
        return in_array($type, $this->types);
    }

    public function isExtendAr(array $types) {
        return !empty(array_uintersect($this->types, $types, function ($a, $b) {
            return $a === $b ? 0 : 1;
        }));
    }
}