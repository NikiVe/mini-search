<?php


namespace MiniSearch\Source;


class ArrayS implements ISource
{
    protected array $words;
    protected int $position = 0;

    public function __construct(array $words) {
        $this->words = $words;
    }

    public function getWord(): string {
        $this->position++;
        return $this->words[$this->position - 1] ?? '';
    }
}