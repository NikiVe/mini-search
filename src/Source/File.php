<?php


namespace MiniSearch\Source;


use MiniSearch\Word;

class File implements ISource
{
    protected array $words;
    protected int $position = 0;
    protected string $strContent;

    protected int $wordLength = 0;
    protected int $contentLen = 0;
    protected string $id;
    protected string $path;

    public function __construct(string $path, string $id = '') {
        $this->path = $path;
        $this->strContent = file_get_contents($path);
        $this->id = $id === '' ? md5($path) : $id;
        $this->contentLen = mb_strlen($this->strContent);
    }

    public static function isServiceSymbol(int $code) {
        return $code < 47 && $code !== 45; // 45 - это тире
    }

    public function getWord(): ?Word {
        $word = '';
        for (; $this->position < $this->contentLen; $this->position++) {
            if (!self::isServiceSymbol(ord($this->strContent[$this->position]))) {
                $word .= $this->strContent[$this->position];
            } elseif (mb_strlen($word)) {
                break;
            }
        }

        if (strlen($word)) {
            $this->wordLength++;
        }
        return $word ? new Word($this->position - mb_strlen($word), $word) : null;
    }

    public function getWordsLength() : int {
        return $this->wordLength;
    }

    public function getPosition(): int {
        return $this->position;
    }

    public function getContent(int $start, int $length): string {
        return substr($this->strContent, $start, $length - $start);
    }

    public function getAllContent(int $start) {
        return substr($this->strContent, $start, $this->contentLen);
    }

    public function getSourceId(): string {
        return $this->id;
    }

    public function testF() {
        $l = mb_strlen($this->strContent);
        $sT = microtime(true);
        $i = '';
        for ($k = 0; $k < $l; $k++) {
            $i .= $this->strContent[$k];
        }
//        while ($s = mb_substr($this->strContent, $this->position++, 1, 'UTF-8')) {
//            $i++;
//        }
        echo 'time: ' . round(microtime(true) - $sT, 4) . PHP_EOL;
    }
}