<?php


namespace MiniSearch\Source;


class FileStore
{
    protected array $fileList = [];

    public function __construct(array $fileList) {
        foreach ($fileList as $num => $item) {
           $this->fileList[$num] = new File($item, $num);
        }
    }

    public function getFileList() : array {
        return $this->fileList;
    }

    public function getFileById(string $id) : File {
        return $this->fileList[$id];
    }
}