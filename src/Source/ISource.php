<?php


namespace MiniSearch\Source;


use MiniSearch\Word;

interface ISource
{
    public function getWord() : ?Word;
    public function getWordsLength() : int;
    public function getPosition() : int;
    public function getContent(int $start, int $countWords) : string;
    public function getSourceId() : string;
}