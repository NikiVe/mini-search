<?php


namespace MiniSearch;


use MiniSearch\Source\FileStore;

class Visualization
{
    protected string $saveDir = '';

    public function __construct(string $saveDir) {
        $this->saveDir = $saveDir;
    }

    public static function getHtmlTemplate () {
        return "<html>
                    <body>
                        <ul>
                            #links#
                        </ul>
                        <div class='result'>
                            #result#
                        </div>
                        
                    </body>
                </html>";
    }

    public static function getLinks(array $ids) {
        ksort($ids);
        $links = '';
        foreach ($ids as $num => $positions) {
            $c = count($positions);
            $links .= "<li><a href='#$num'>Выдача №$num ($c)</a></li>";
        }
        return $links;
    }

    protected function prepareContent(array $content) {
        $num = 0;
        $result = [];
        foreach ($content as $fileName => $arGroups) {
            krsort($arGroups);
            foreach ($arGroups as $groups) {
                foreach ($groups as $count => $arPositions) {
                    foreach ($arPositions as $position) {
                        $result[$fileName][] = ['id' => $num, 'position' => $position->position];
                    }
                    $num++;
                }
            }
            uasort($result[$fileName], function (array $a, array $b) {
                return $a['position'] > $b['position'];
            });
        }

        return $result;
    }

    public function saveAsHtml(array $content, FileStore $fileStore) {
        $content = $this->prepareContent($content);

        foreach ($content as $fileName => $arPosition) {
            $resultContent = '';
            $file = $fileStore->getFileById($fileName);
            $start = 0;
            $ids = [];
            foreach ($arPosition as $position) {
                $id = $position['id'];
                $ids[$id][] = $position['position'];
                $resultContent .= $file->getContent($start, $position['position']) . "<span id='$id'>-$id-></span>";
                $start = $position['position'];
            }
            $resultContent .= $file->getAllContent($start);
            $resultContent = str_replace('#result#', $resultContent, self::getHtmlTemplate());
            $resultContent = str_replace('#links#', self::getLinks($ids), $resultContent);
            file_put_contents($this->saveDir . "/result_$fileName.html", $resultContent);
        }
    }
}