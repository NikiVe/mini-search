<?php
namespace MiniSearch;

use cijic\phpMorphy\Morphy;

class Weigh
{
    protected array $profile = [];
    protected Morphy $morphy;

    public function __construct(Morphy $morphy, array $profile = []) {
        $this->morphy = $morphy;
        $this->profile = $profile ?: self::getDefaultProfile();
    }

    /**
     * Получить вес (значимость) слова
     *
     * @param string $word
     * @return int
     */
    public function get(string $word) {
        $range = [];
        $word = mb_strtoupper($word);

        $partOfSpeech = $this->morphy->getPartOfSpeech($word);
        if (!$partOfSpeech) {
            return $this->profile['DEFAULT'];
        }

        foreach ($partOfSpeech as $part) {
            $range[] = $this->profile[$part] ?? $this->profile['DEFAULT'];
        }

        return max($range);
    }

    public static function getDefaultProfile() {
        return [
            'ПРЕДЛ' => 0,
            'СОЮЗ'  => 0,
            'МЕЖД'  => 0,
            'ВВОДН' => 0,
            'ЧАСТ'  => 0,
            'МС'    => 0,

            'С'     => 5,
            'Г'     => 5,
            'П'     => 3,
            'Н'     => 3,

            'DEFAULT' => 1
        ];
    }
}