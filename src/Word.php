<?php


namespace MiniSearch;


class Word
{
    public int $position;
    public string $word;

    public function __construct(int $position, string $word) {
        $this->position = $position;
        $this->word = $word;
    }
}